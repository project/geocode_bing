<?php // $Id$

/**
 * @file
 * Extends the geocode class from the geocode module http://drupal.org/project/geocode
 */

class geocode_bing extends geocode {
  var $key = '';

  function set_key() {
      $this->key = variable_get('bingmaps_api_key', '');
  }
  private $server = 'http://dev.virtualearth.net/webservices/v1';

  function set_result($result) {
    $this->result = $result;
    $addr = $result->Address;
    $this->country_name = $addr->CountryRegion;
    $this->state = $addr->District;
    $this->city = $addr->PostalTown;
    $this->street1 = $addr->AddressLine;
    $this->zip = $addr->PostalCode;
    $this->point = array(
        'type' => 'point', 'lat' => $result->Locations->GeocodeLocation->Latitude, 'lon' => $result->Locations->GeocodeLocation->Longitude, 'alt' => $result->Locations->GeocodeLocation->Altitude,
    );
  }

  function geocode($input, $n = 1) {

  /*
    TODO Make the array work for more than one value.
  */
      if (is_array($input)) {
    /*
      $count = count($input);
      foreach ($input as $key => $val)  {
        $input .= $val['value'];
        if ($key != ($count-1)) $input .=  ",";
      }
      */
      $input = $input['value'];
    }
  if (empty($this->key)) $this->set_key();
  //if (is_array($input)) $input = join(', ', $input);

  /*
    TODO Check that the WSDL is actually the there and return a lovely Drupal error if not.
  */
    $client = new SoapClient($this->server . '/geocodeservice/geocodeservice.svc?wsdl');
    $request = $this->request($input, $n);
    $result = $client->Geocode($request);

  // Check to see if the geocoding was successful.
  if ($result->GeocodeResult->ResponseSummary->StatusCode == 'Success') {
    // If the geocoder is unsure, it returns multiple results as an array, otherwise it returns just the one result. Check for this.
    if (is_array($result->GeocodeResult->Results->GeocodeResult)) {
      error_log('array');
      $this->set_result($result->GeocodeResult->Results->GeocodeResult[0]);
    }
    else {
      $this->set_result($result->GeocodeResult->Results->GeocodeResult);
    }
      return TRUE;
    }

    return FALSE;
}

  private function request($input, $n) {
    if (empty($this->key)) $this->set_key();
    return array('request' => array(
      'Credentials' => array('ApplicationId' => $this->key),
      'Query' => $input,
      'Options' => array('Count' => $n),
    ));
  }

}
