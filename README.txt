
Description
-----------

This module plugs into the Geocode module (http://drupal.org/project/geocode) to allow you to use Bing maps to geocode your information. When used in conjunction with Geo Field (http://drupal.org/project/geo) you can geocode your CCK fields.

Installation
------------

* Enable the module as you would with any other module.

* You will need a Bing Maps API key. You can get one from https://www.bingmapsportal.com/application/ 

* Enter your Bing Maps API key on the settings page (admin/settings/geocode_bing).

* Create a CCK field of type 'Geospatial data' and operation 'Geocoded value from another field'.

* On the field settings page, select the CCK fields you want to Geocode & use the 'Geocode handler' type of 'Bing'.

* Have lots of fun. 